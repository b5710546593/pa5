import java.util.Calendar;

/**
 * State for set minute on alert time.
 * @author Voraton Lertrattanapaisal
 *
 */
public class SetMinuteState implements State {

	private ClockUI clock;
	
	/**
	 * To initialize SetMinuteState.
	 * @param clock is ClockUI that called SetMinuteState.
	 */
	public SetMinuteState(ClockUI clock) {
		this.clock = clock;
		action();
	}
	
	@Override
	public void action() {
		clock.displayAlertTime();
	}

	@Override
	public void actionPlus() {
		int minute = (clock.alertTime.get(Calendar.MINUTE)+1)%60;
		clock.setAlert(Calendar.MINUTE,minute);
	}

	@Override
	public void actionMinus() {
		int minute = (clock.alertTime.get(Calendar.MINUTE)+59)%60;
		clock.setAlert(Calendar.MINUTE,minute);
	}

	@Override
	public void leaveState() {
		clock.setState(new SetSecondState(clock));
	}

}
