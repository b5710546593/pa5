
/**
 * State for alert user.
 * @author Voraton Lertrattanapaisal
 *
 */
public class AlertState implements State {

	private ClockUI clock;

	/**
	 * To initialize AlertState.
	 * @param clock is ClockUI that called AlertState.
	 */
	public AlertState(ClockUI clock) {
		this.clock = clock;
		action();
	}
	
	@Override
	public void action() {
		clock.alertTimer.start();
	}

	@Override
	public void actionPlus() {
		leaveState();
	}

	@Override
	public void actionMinus() {
		leaveState();
	}

	@Override
	public void leaveState() {
		clock.sound.stop();
		clock.alertTimer.stop();
		clock.stop();
		clock.setState(new DisplayTimeState(clock));
	}

}
