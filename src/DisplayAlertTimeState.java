
/**
 * State for show alert time.
 * @author Voraton Lertrattanapaisal
 *
 */
public class DisplayAlertTimeState implements State {

	private ClockUI clock;
	
	/**
	 * To initialize DisplayAlertTimeState.
	 * @param clock is ClockUI that called DisplayAlertTimeState.
	 */
	public DisplayAlertTimeState(ClockUI clock) {
		this.clock = clock;
		action();
	}
	
	@Override
	public void action() {
		clock.displayAlertTime();
	}

	@Override
	public void actionPlus() {
		leaveState();
	}

	@Override
	public void actionMinus() {
		leaveState();
	}

	@Override
	public void leaveState() {
		clock.setState(new DisplayTimeState(clock));
	}

}
