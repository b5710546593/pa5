/**
 * For running the ClockUI.
 * @author Voraton Lertrattanapaisal
 *
 */
public class Main {
	/**
	 * Main for run the ClockUI.
	 * @param args do nothing.
	 */
	public static void main(String[] args) {
		ClockUI clock = new ClockUI();
		clock.run();
	}
}
