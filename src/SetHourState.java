import java.util.Calendar;

/**
 * State for set hour on alert time.
 * @author Voraton Lertrattanapaisal
 *
 */
public class SetHourState implements State {

	private ClockUI clock;
	
	/**
	 * To initialize SetHourState.
	 * @param clock is ClockUI that called SetHourState.
	 */
	public SetHourState(ClockUI clock) {
		this.clock = clock;
		action();
	}
	
	@Override
	public void action(){
		clock.displayAlertTime();
	}
	
	@Override
	public void actionPlus(){
		int hour = (clock.alertTime.get(Calendar.HOUR_OF_DAY)+1)%24;
		clock.setAlert(Calendar.HOUR_OF_DAY,hour);
	}
	
	@Override
	public void actionMinus(){
		int hour = (clock.alertTime.get(Calendar.HOUR_OF_DAY)+23)%24;
		clock.setAlert(Calendar.HOUR_OF_DAY,hour);
	}
	
	@Override
	public void leaveState(){
		clock.setState(new SetMinuteState(clock));
	}

}
