
/**
 * State for display current time and update time.
 * @author Voraton Lertrattanapaisal
 *
 */
public class DisplayTimeState implements State {
	
	private ClockUI clock;
	
	/**
	 * To initialize DisplayTimeState.
	 * @param clock is ClockUI that called DisplayTimeState.
	 */
	public DisplayTimeState(ClockUI clock){
		this.clock = clock;
		action();
	}
	
	@Override
	public void action(){
		clock.updateTimer.start();
	}
	
	@Override
	public void actionPlus(){
		clock.setState(new DisplayAlertTimeState(clock));
	}
	
	@Override
	public void actionMinus(){
		//do nothing
	}
	
	@Override
	public void leaveState(){
		clock.updateTimer.stop();
		clock.setState(new SetHourState(clock));
	}
}
