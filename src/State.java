/**
 * State interface for state machine.
 * @author Voraton Lertrattanapaisal.
 *
 */
public interface State {
	/**
	 * For do the action of ClockUI.
	 */
	public void action();
	/**
	 * For do some action when the plusButton press.
	 */
	public void actionPlus();
	/**
	 * For do some action when the minusButton press.
	 */
	public void actionMinus();
	/**
	 * For leave the state to another state.
	 */
	public void leaveState();
}
