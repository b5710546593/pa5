import java.util.Calendar;

/**
 * State for set second on alert time.
 * @author Voraton Lertrattanapaisal
 *
 */
public class SetSecondState implements State {
	
	private ClockUI clock;
	
	/**
	 * To initialize SetSecondState.
	 * @param clock is ClockUI that called SetSecondState.
	 */
	public SetSecondState(ClockUI clock) {
		this.clock = clock;
		action();
	}
	
	@Override
	public void action() {
		clock.displayAlertTime();
	}

	@Override
	public void actionPlus() {
		int second = (clock.alertTime.get(Calendar.SECOND)+1)%60;
		clock.setAlert(Calendar.SECOND,second);
	}

	@Override
	public void actionMinus() {
		int second = (clock.alertTime.get(Calendar.SECOND)+59)%60;
		clock.setAlert(Calendar.SECOND,second);
	}

	@Override
	public void leaveState() {
		clock.setState(new DisplayTimeState(clock));
	}

}
