import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Clock UI is graphic user interface which is alarm clock.
 * @author Voraton Lertrattanapaisal
 *
 */
public class ClockUI extends JFrame {
	
	private State state;
	private JLabel hour1;
	private JLabel hour2;
	private JLabel colon1;
	private JLabel min1;
	private JLabel min2;
	private JLabel colon2;
	private JLabel sec1;
	private JLabel sec2;
	private JLabel alarm;
	Clip sound;
	private ImageIcon colonImage;
	private ImageIcon[] digitNums;
	private ImageIcon blank;
	private boolean isAlarm;
    private Calendar currentTime;
    Calendar alertTime;
    
	int delay = 500; // milliseconds
	/**
	 * taskAlert for make clock blinking when alarm.
	 */
	ActionListener taskAlert = new ActionListener() {
		public void actionPerformed(ActionEvent event){ 
			alert();
		}
	};
	/**
	 * alertTimer is used for alerting.
	 */
	javax.swing.Timer alertTimer = new javax.swing.Timer(delay, taskAlert);
	/**
	 * taskTimer for update real time.
	 */
	ActionListener taskTimer = new ActionListener() {
		public void actionPerformed(ActionEvent event){ 
			updateTime();
		}
	};
	/**
	 * updateTimer is used for update time.
	 */
	javax.swing.Timer updateTimer = new javax.swing.Timer(delay, taskTimer);

	/**
	 * setState is used for change the state.
	 * @param newState is new state that will be changed.
	 */
	public  void setState(State newState){
		this.state = newState;
	}
	
	/**
	 * Constructor for initialize ClockUI.
	 */
	public ClockUI (){
		super("Cheap Clock");
		currentTime = Calendar.getInstance();
		isAlarm = false;
		initAlert();
		loadMedia();
		state = new DisplayTimeState(this);
		initComponents();
	}
	
	/**
	 * For initialize components on JFrame.
	 */
	public void initComponents(){
		Container contain = getContentPane();
		contain.setLayout(new GridLayout(2,1));
		contain.setBackground(Color.BLACK);
		
		Container upper = new Container();
		upper.setLayout(new FlowLayout());
		
		hour1 = new JLabel ();
		hour2 = new JLabel ();
		colon1 = new JLabel(colonImage);
		min1 = new JLabel();
		min2 = new JLabel();
		colon2 = new JLabel(colonImage);
		sec1 = new JLabel();
		sec2 = new JLabel();
		alarm = new JLabel(" A ");
		alarm.setForeground(Color.GRAY);
		
		upper.add(hour1);
		upper.add(hour2);
		upper.add(colon1);
		upper.add(min1);
		upper.add(min2);
		upper.add(colon2);
		upper.add(sec1);
		upper.add(sec2);
		upper.add(alarm);
		
		
		Container lower = new Container();
		lower.setLayout(new FlowLayout());
		
		JButton set = new JButton("SET");
		set.setBackground(Color.BLACK);
		set.setForeground(Color.ORANGE);
		set.addActionListener(new SetButtonListener());
		
		JButton plus = new JButton("+");
		plus.setBackground(Color.BLACK);
		plus.setForeground(Color.ORANGE);
		plus.addActionListener(new PlusButtonListener());
		
		JButton minus = new JButton("-");
		minus.setBackground(Color.BLACK);
		minus.setForeground(Color.ORANGE);
		minus.addActionListener(new MinusButtonListener());
				
		lower.add(set);
		lower.add(minus);
		lower.add(plus);
		
		contain.add(upper);
		contain.add(lower);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * For initialize alert time.
	 */
	public void initAlert(){
		alertTime = Calendar.getInstance();
		alertTime.set(Calendar.HOUR_OF_DAY, 0);
		alertTime.set(Calendar.MINUTE, 0);
		alertTime.set(Calendar.SECOND, 0);
	}
	
	/**
	 * For load media such as images or sound to the ClockUI.
	 */
	public void loadMedia() { 
		try{
			sound = AudioSystem.getClip();
			Class classLoader = this.getClass();
			URL blankURL = classLoader.getResource("images/blank.gif");
			URL colon =  classLoader.getResource("images/colon_s.gif");
			URL audio = ClassLoader.getSystemResource("sound/alertsound.wav");
			AudioInputStream soundSteam = AudioSystem.getAudioInputStream(audio);
			sound.open(soundSteam);
			colonImage = new ImageIcon(colon);
			blank = new ImageIcon(blankURL);
			digitNums = new ImageIcon[10];
	        for (int i = 0 ; i < digitNums.length; i++){
	        	URL image =  classLoader.getResource("images/"+i+"s.gif");
	    		ImageIcon digit = new ImageIcon(image);
	    		digitNums[i] = digit;
	        }
		}
		catch (IOException | UnsupportedAudioFileException | LineUnavailableException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * For update current time.
	 */
	public void updateTime(){
        currentTime.setTimeInMillis( System.currentTimeMillis() );
        int h1 = currentTime.get( Calendar.HOUR_OF_DAY )/10;
        int h2 = currentTime.get( Calendar.HOUR_OF_DAY )%10;
        int m1 = currentTime.get( Calendar.MINUTE )/10;
        int m2 = currentTime.get( Calendar.MINUTE )%10;
        int s1 = currentTime.get( Calendar.SECOND )/10;
        int s2 = currentTime.get( Calendar.SECOND )%10;
        hour1.setIcon(digitNums[h1]);
        hour2.setIcon(digitNums[h2]);
        min1.setIcon(digitNums[m1]);
        min2.setIcon(digitNums[m2]);
        sec1.setIcon(digitNums[s1]);
        sec2.setIcon(digitNums[s2]);

        if (checkAlert()){
        	setState(new AlertState(this));
        }
        else {
			pack();
	        state.action();
        }
	}
	
	/**
	 * For setting alert time.
	 * @param Type is field that will be set.
	 * @param value is amount that will be set on the field.
	 */
	public void setAlert(int Type,int value){
		isAlarm = true;
		alarm.setForeground(Color.ORANGE);
		alertTime.set(Type, value);
		state.action();
	}
	
	public void stop(){
		alarm.setForeground(Color.GRAY);
		isAlarm = false;
	}
	
	/**
	 * For display alert time that was set.
	 */
	public void displayAlertTime(){
		int h1 = alertTime.get( Calendar.HOUR_OF_DAY )/10;
        int h2 = alertTime.get( Calendar.HOUR_OF_DAY )%10;
        int m1 = alertTime.get( Calendar.MINUTE )/10;
        int m2 = alertTime.get( Calendar.MINUTE )%10;
        int s1 = alertTime.get( Calendar.SECOND )/10;
        int s2 = alertTime.get( Calendar.SECOND )%10;
        hour1.setIcon(digitNums[h1]);
        hour2.setIcon(digitNums[h2]);
        min1.setIcon(digitNums[m1]);
        min2.setIcon(digitNums[m2]);
        sec1.setIcon(digitNums[s1]);
        sec2.setIcon(digitNums[s2]);
		pack();
	}
	
	/**
	 * For checking alert time and current time is it same.
	 * @return true if current time == alert time.
	 */
	public boolean checkAlert(){
		int currentHour = currentTime.get( Calendar.HOUR_OF_DAY );
		int currentMinute = currentTime.get( Calendar.MINUTE );
		int currentSecond = currentTime.get( Calendar.SECOND );
		int alertHour = alertTime.get( Calendar.HOUR_OF_DAY );
		int alertMinute = alertTime.get( Calendar.MINUTE );
		int alertSecond = alertTime.get( Calendar.SECOND );
		if (currentHour == alertHour && currentMinute == alertMinute && currentSecond == alertSecond && isAlarm) return true;
		return false;
	}
	
	/**
	 * For alerting user by blinking LED and ringing.
	 */
	public void alert(){
        currentTime.setTimeInMillis( System.currentTimeMillis() );
        int s2 = currentTime.get( Calendar.SECOND )%10;
        if (s2%2==0){
        	hour1.setIcon(blank);
        	hour2.setIcon(blank);
        	min1.setIcon(blank);
        	min2.setIcon(blank);
        	sec1.setIcon(blank);
        	sec2.setIcon(blank);
        }
        else {
        	updateTime();
        }
		sound.loop(sound.LOOP_CONTINUOUSLY);
		sound.start();
        state.action();
	}
	
	/**
	 * ActionListener for setButton.
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	class SetButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			state.leaveState();
		}
	}
	
	/**
	 * ActionListener for plusButton.
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	class PlusButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			state.actionPlus();
		}
	}
	
	/**
	 * ActionListener for minusButton.
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	class MinusButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			state.actionMinus();
		}
	}
	
	/**
	 * For run the ClockUI.
	 */
	public void run(){
		setVisible(true);
	}

}
